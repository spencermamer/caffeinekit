//
//  Drink.swift
//  Cups of Joe
//
//  Created by Spencer Mamer on 1/11/16.
//  Copyright © 2016 Spencer Mamer. All rights reserved.
//

import Foundation

final public class Drink: NSObject, NSCoding, NSCopying {
    
    
    /**
     String constants that are used to archive the stored properties of a `List`. These constants
     are used to help implement `NSCoding`.
     */
    private struct SerializationKeys {
        static let Name = "name"
        static let Caffeine = "caffeine"
        static let UUID = "uuid"
    }
    
    public var uuid : String = NSUUID().UUIDString
    public var name: String
    public var milligrams: Double
    
    public  init(name: String, milligrams: Double, uuid: String) {
        self.name = name
        self.milligrams = milligrams
        self.uuid = uuid
    }
    
    public  convenience init(name: String, milligrams: Double) {
        self.init(name: name, milligrams: milligrams, uuid: NSUUID().UUIDString)
    }
    
    public required convenience init?(coder decoder: NSCoder) {
        guard let name = decoder.decodeObjectForKey(SerializationKeys.Name) as? String, uuid = decoder.decodeObjectForKey(SerializationKeys.UUID) as? String
            else {return nil}
        self.init(
            name: name,
            milligrams: decoder.decodeDoubleForKey(SerializationKeys.Caffeine),
            uuid: uuid
        )
        
    }
    
    public func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.name, forKey: SerializationKeys.Name)
        coder.encodeDouble(self.milligrams, forKey: SerializationKeys.Caffeine)
        coder.encodeObject(self.uuid, forKey: SerializationKeys.UUID)
    }
    
    // MARK: NSCopying
    
    public func copyWithZone(zone: NSZone) -> AnyObject  {
        return Drink(name: name, milligrams: milligrams, uuid: uuid)
    }
    
}


