# CaffeineKit

[![CI Status](http://img.shields.io/travis/Spencer Mamer/CaffeineKit.svg?style=flat)](https://travis-ci.org/Spencer Mamer/CaffeineKit)
[![Version](https://img.shields.io/cocoapods/v/CaffeineKit.svg?style=flat)](http://cocoapods.org/pods/CaffeineKit)
[![License](https://img.shields.io/cocoapods/l/CaffeineKit.svg?style=flat)](http://cocoapods.org/pods/CaffeineKit)
[![Platform](https://img.shields.io/cocoapods/p/CaffeineKit.svg?style=flat)](http://cocoapods.org/pods/CaffeineKit)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CaffeineKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CaffeineKit"
```

## Author

Spencer Mamer, spencermamer@gmail.com

## License

CaffeineKit is available under the MIT license. See the LICENSE file for more info.
